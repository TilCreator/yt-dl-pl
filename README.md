# Youtube Playlist Offline Sync

Simple Python script to sync Youtube playlists for offline use.

## Install and Use
- Download `youtubeSync.py` into your music directory
- Get `python`
- Get `youtube-dl`, `eyed3`, `tqdm` (Python modules)
- Create folders `yt`, `playlists` in your music directory
- Create files `playlists/▶ <playlist name> #<playlist id>.m3u`, for each playlist you want to sync
  - <playlist name>: Name the playlist will be called, not necessarily to YT playlist name
  - <playlist id>: Has to be extracted form an YT url, the `list` GET parameter
  - (ignore <>)
- Execute script: `$ python youtubeSync.py`
  - This populates the playlist with file name
  - and downloads the files into the `yt` directory

You can still add playlist in the `playlists` directory and sync with `$ python youtubeSync.py`.

## How does it work
- Get playlists form `playlists` directory and already downloaded files from `yt`
- Get playlsit video ids from Youtube with youtube-dl
- Download and delete videos audio to/from `yt` directory
  - Each file is its own album, this is to force music player to display the thumbnail
- Update content of playlists in `playlists` directory
