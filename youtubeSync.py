#!/bin/env python3
import os
import re
import sys
import eyed3
import youtube_dl
from tqdm import tqdm


DISABLE_RM = True


class logger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        self.error(msg)

    def error(self, msg):
        pbar.write(f'ERR: {msg} ({id})')


def downloadHook(d):
    def sizeof_fmt(num, suffix='B'):
        if not isinstance(num, str):
            for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
                if abs(num) < 1024.0:
                    return "%3.1f%s%s" % (num, unit, suffix)
                num /= 1024.0
            return str("%.1f%s%s" % (num, 'Yi', suffix))
        else:
            return str(num)

    if d['status'] == 'finished':
        pbar.set_description(f'Converting {id}')

    if d['status'] == 'downloading':
        if '_total_bytes_str' not in d:
            if 'total_bytes' in d:
                d['_total_bytes_str'] = sizeof_fmt(d['total_bytes'])
            elif 'total_bytes_estimate' in d:
                d['_total_bytes_str'] = sizeof_fmt(d['total_bytes_estimate'])
            else:
                d['_total_bytes_str'] = 'unknown'
        if '_percent_str' not in d:
            d['_percent_str'] = ' unknown'
        if '_speed_str' not in d:
            d['_speed_str'] = 'unknown'
        if '_eta_str' not in d:
            d['_eta_str'] = 'unknown'
        if 'downloaded_bytes' not in d:
            d['downloaded_bytes'] = 'unknown'

        pbar.set_description(f'Downloading {id}')
        # pbar.write('[' + d['_percent_str'].replace(' ', '') + ' ' + sizeof_fmt(d['downloaded_bytes']) + '/' + d['_total_bytes_str'] + ' ' + d['_speed_str'] + ' ETA ' + d['_eta_str'] + ']', end='\r')


def get_diff(array1, array2):
    output = []
    for value in array1:
        if value not in array2:
            output.append(value)

    return output


eyed3.log.setLevel("ERROR")

os.chdir(os.path.dirname(os.path.realpath(__file__)))

if os.environ.get("SHELL") is not None:
    pbar_file = sys.stdout
else:
    pbar_file = open('/dev/null', 'w')

playlists = {}
for name in os.listdir('playlists'):
    match = re.match(r'(▶.*) #([a-zA-Z0-9_-]+)\.m3u', name)
    if match:
        playlists[match.groups()[1]] = {'name': match.groups()[0]}

print(f'Found {len(playlists)} playlists')


local_files = []
for name in os.listdir('yt'):
    match = re.match(r'▶([a-zA-Z0-9_-]+)\.mp3', name)
    if match:
        local_files.append(match.groups()[0])


print(f'Found {len(local_files)} local files')


with open(os.path.join('playlists', 'to_ignore')) as f:
    to_ignore = [line.replace('\n', '') for line in f.readlines()]


pbar = tqdm(playlists, desc='Getting playlist info', file=pbar_file)
for id in pbar:
    pbar.set_description(f'Getting {playlists[id]["name"]}\'s (#{id}) info')
    pbar.status_printer('test')

    with youtube_dl.YoutubeDL({'logger': logger(),
                               'ignoreerrors': True,
                               'geo_bypass': True,
                               'retries': 5,
                               'socket_timeout': 2000,
                               'cachedir': False,
                               'extract_flat': 'in_playlist'}) as ydl:
        resultRaw = ydl.extract_info(id)

    playlists[id]['files'] = []
    for videoData in resultRaw['entries']:
        if videoData['title'] != '[Deleted video]' and videoData['title'] != '[Private video]' and videoData['id'] not in to_ignore:
            playlists[id]['files'].append(videoData['id'])

    pbar.set_description('Got playlist info')


to_cleanup = []
files = [os.path.join(dire, file) for dire in ['yt', 'playlists']
         for file in os.listdir(dire)]  # os.path.join('playlists', 'mpd')
for file in files:
    name = file[file.find('/'):]
    if re.match(r'.*\.mp3\.part', file) or \
       re.match(r'.*\.jpg', file) or re.match(r'.*\.tmp', file) or \
       re.match(r'(.*)\.sync-conflict-[0-9]+-[0-9]+-[A-Z0-9]+(\..*)', file):
        to_cleanup.append(file)


online_files = [id for p_id in playlists.keys()
                for id in playlists[p_id]['files']]


to_delete = get_diff(local_files, online_files)
to_download = get_diff(online_files, local_files)


print('Playlist info:')
for id in playlists:
    tmp_local_files = [
        v_id for v_id in local_files if v_id in playlists[id]['files']]
    print(
        f' {playlists[id]["name"]} (#{id}) ({len(playlists[id]["files"])} files ({len(tmp_local_files)} local))')


print(('TODO:\n'
       f' - Delete {len(to_delete)} files\n'
       f' - Download {len(to_download)} files\n'
       f' - Cleanup {len(to_cleanup)} files'))


if len(to_delete) > 0:
    if DISABLE_RM:
        print('Not deleting anything')
        print(f'Would delete: {" ".join(to_delete)}')
    else:
        pbar = tqdm(to_delete, desc='Deleting files', file=pbar_file)
        for id in pbar:
            pbar.set_description(f'Deleting {id}')
            os.remove(os.path.join('yt', f'▶{id}.mp3'))
            pbar.set_description(f'Deleted {len(to_delete)} files')


if len(to_cleanup) > 0:
    pbar = tqdm(to_cleanup, desc='Cleaning up files', file=pbar_file)
    for file in pbar:
        pbar.set_description(f'Cleaning up {file}')
        os.remove(file)

        pbar.set_description(f'Cleaned up {len(to_cleanup)} files')


if len(to_download) > 0:
    pbar = tqdm(to_download, desc='Downloading files', file=pbar_file)
    for id in pbar:
        pbar.set_description(f'Downloading {id}')

        with youtube_dl.YoutubeDL({'outtmpl': os.path.join('yt', '▶%(id)s.%(ext)s'),
                                   'format': 'bestaudio/best',
                                   'postprocessors': [
                                       {
                                           'key': 'FFmpegVideoConvertor',
                                           'preferedformat': 'mp3'
                                       },
                                       {'key': 'EmbedThumbnail'},
                                       {'key': 'FFmpegMetadata'}],
                                   'writethumbnail': True,
                                   'logger': logger(),
                                   'geo_bypass': True,
                                   'ignoreerrors': True,
                                   'retries': 5,
                                   'socket_timeout': 2000,
                                   'cachedir': False,
                                   'progress_hooks': [downloadHook]}) as ydl:
            ydl.extract_info(id)

        if os.path.exists(os.path.join('yt', f'▶{id}.mp3')):
            tags = eyed3.load(os.path.join('yt', f'▶{id}.mp3'))
            tags.tag.album = f'▶{id}'
            tags.tag.save()

        pbar.set_description(f'Downloaded {len(to_download)} files')


for file in os.listdir(os.path.join('playlists', 'mpd')):
    if re.match(r'(▶.*) #([a-zA-Z0-9_-]+)\..+', file):
        os.remove(os.path.join('playlists', 'mpd', file))
        os.remove(os.path.join('playlists', 'mpd', 'lab23', file))

for id in playlists:
    with open(os.path.join('playlists', f'{playlists[id]["name"]} #{id}.m3u'), 'w') as f:
        f.write(
            '\n'.join([f'../yt/▶{v_id}.mp3' for v_id in playlists[id]['files']]))
    with open(os.path.join('playlists', 'mpd', f'{playlists[id]["name"]} #{id}.m3u'), 'w') as f:
        f.write(
            '\n'.join([f'yt/▶{v_id}.mp3' for v_id in playlists[id]['files']]))
    with open(os.path.join('playlists', 'mpd', 'lab23', f'{playlists[id]["name"]} #{id}.m3u'), 'w') as f:
        f.write(
            '\n'.join([f'tils/yt/▶{v_id}.mp3' for v_id in playlists[id]['files']]))
